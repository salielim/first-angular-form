(function() {
    var MyApp = angular.module("MyApp", []);

    var inputFields = ["name", "email", "phone", "comment", "experience"]

    var MyCtrl = function() {
        var myCtrl = this;

        myCtrl.clearForm = function() {
            for (var i in inputFields) {
                myCtrl[inputFields[i]] = "";
            }
        }

        myCtrl.processForm = function() {
            for (var i in inputFields) {
                console.log("%s: %s", inputFields[i], myCtrl[inputFields[i]]);
            }
        }
            /* or use
            for (var i in myCtrl) 
                if (typeof (myCtrl[i] == "string"))
                    console.log("input: %s = %s", i, myCtrl[i]);
                myCtrl.clearForm(); */
        
        myCtrl.isValid = function (myForm, fieldName) {
            return (myForm[fieldName].$invalid && myForm[fieldName].$dirty);
        }
    };

    MyApp.controller("MyCtrl", MyCtrl);
})();